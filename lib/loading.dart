import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pingpong_schedule/globals.dart' as globals;

pingpongLoader(BuildContext context) {
  if (MediaQuery.of(context).platformBrightness.toString() == Brightness.dark.toString() || globals.darkMode == true) {
    return FlareActor("assets/PingPongLoadingLight.flr", animation:"Loading", sizeFromArtboard: true);
  } else {
    return FlareActor("assets/PingPongLoading.flr", animation:"Loading", sizeFromArtboard: true);
  }
}