import 'package:flutter/material.dart';
import 'package:pingpong_schedule/mainPages/schedule.dart';
import 'package:pingpong_schedule/mainPages/settings.dart';
import 'package:pingpong_schedule/mainPages/tests.dart';
import 'package:pingpong_schedule/mainPages/messages.dart';
import 'package:pingpong_schedule/styles.dart';
import 'package:pingpong_schedule/app_localizations.dart';
import 'dart:convert' as convert;
import 'package:pingpong_schedule/globals.dart' as globals;

// Application or Landing Page
class Application extends StatefulWidget {
  Application({Key key}) : super(key: key);

  @override
  _ApplicationState createState() => _ApplicationState();
}

class _ApplicationState extends State<Application> {
  // For the BottomNavigationBar
  int _selectedPageIndex = 0; //The app starts up with the homePage (index 0) as the open widget
  var _courses;
  var _coursess;
  bool _hawawa;
  static List<Widget> _pageOptions = <Widget>[
    Schedule(),
    Messages(),
    Tests(),
    Settings(),
  ];

  void _onPageItemTapped(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  BottomNavigationBarItem bottomNavBarItem(BuildContext context, IconData icon, String title) {
    return BottomNavigationBarItem(
      icon: Icon(icon),
      title: Text(
        title,
        style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
      ),
    );
  }
  loadThing() async {
    String data = await DefaultAssetBundle.of(context).loadString("assets/courses.json");
    _courses = convert.jsonDecode(data) ?? [];
    String moreData = await DefaultAssetBundle.of(context).loadString("assets/reverse-courses.json");
    _coursess = convert.jsonDecode(moreData) ?? [];
    _hawawa = globals.prefs.getBool("darktheme") ?? false;
    setState(() {
      globals.courses = _courses;
      globals.reverseCourses = _coursess;
      globals.darkMode = _hawawa;
    });
  }

  initState() {
    super.initState();
    loadThing();
  }

  @override
  Widget build(BuildContext context) {
    if (globals.courses == null) {
      return Container();
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(translated(context, 'appbar_title')),
      ),
      body: Animate.fadeSwitcher(_pageOptions.elementAt(_selectedPageIndex)),
      // The BottomNavigationBar
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          bottomNavBarItem(context, Icons.calendar_today, translated(context, 'bottomnavbar_schedule')),
          bottomNavBarItem(context, Icons.message, translated(context, 'bottomnavbar_messages')),
          bottomNavBarItem(context, Icons.book, translated(context, 'bottomnavbar_tests')),
          bottomNavBarItem(context, Icons.settings, translated(context, 'bottomnavbar_settings')),
        ],
        currentIndex: _selectedPageIndex,
        onTap: _onPageItemTapped,
      ),
    );
  }
}