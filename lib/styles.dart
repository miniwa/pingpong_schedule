import 'dart:async';
import 'package:flutter/material.dart';
import 'package:pingpong_schedule/globals.dart' as globals;

class Styles {
  static const bottomNavBarItemStyle =
      TextStyle(fontSize: 15, fontWeight: FontWeight.bold);
}

class Animate {
  static fadeSwitcher(Widget _plum) {
    return AnimatedSwitcher(
      duration: const Duration(milliseconds: 200),
      transitionBuilder: (Widget child, Animation<double> animation) {
        return FadeTransition(child: child, opacity: animation);
      },
      child: _plum,
    );
  }
}

class Themes {
  final ThemeData lightTheme = ThemeData(
    brightness: Brightness.light,
    primarySwatch: Colors.purple,
    accentColor: Colors.purpleAccent,
    tabBarTheme: TabBarTheme(
      labelColor: Colors.black,
    ),
  );

  final ThemeData greyTheme = ThemeData(
    brightness: Brightness.dark,
    primarySwatch: Colors.purple,
    scaffoldBackgroundColor: Color.fromRGBO(30, 30, 30, 1),
    accentColor: Colors.purpleAccent,
    appBarTheme: AppBarTheme(
      color: Colors.grey[800], //top bar color
    ),
    canvasColor: Color.fromRGBO(30, 30, 30, 1),
  );

  final ThemeData darkTheme = ThemeData(
    brightness: Brightness.dark,
    primarySwatch: Colors.purple,
    scaffoldBackgroundColor: Colors.black, //backgroubd color
    accentColor: Colors.purpleAccent,
    appBarTheme: AppBarTheme(
      color: Colors.purple, //top bar color
    ),
    canvasColor: Colors.black, //bottomnavbar bgcolor
  );
}

class DarkBloc {
  final _themeController =
      StreamController<bool>(); //TODO: figure out how to make this not complain
  get changeTheme => _themeController.sink.add;
  get darkThemeEnabled => _themeController.stream;
}

class GreyBloc {
  final _themeController =
      StreamController<bool>();
  get changeTheme => _themeController.sink.add;
  get greyThemeEnabled => _themeController.stream;
}

final themeBloc = DarkBloc();
final greyBloc = GreyBloc();

class CIcons {
  static const _kFontFam = 'MyFlutterApp';

  static const IconData leaf = const IconData(0xe800, fontFamily: _kFontFam);
  static const IconData religious_christian = const IconData(0xe82f, fontFamily: _kFontFam);
  static const IconData beaker = const IconData(0xf0c3, fontFamily: _kFontFam);
  static const IconData code = const IconData(0xf121, fontFamily: _kFontFam);
  static const IconData cube = const IconData(0xf1b2, fontFamily: _kFontFam);
  static const IconData calc = const IconData(0xf1ec, fontFamily: _kFontFam);
}

courseName(String name) {
  String _shortName;
  String _longName;
  try {
    _shortName = name.toString().split(",")[0];
    _longName = name.toString().split(",")[1];
  } catch (e) {
    _shortName = name.toString();
    _longName = name.toString();
  }
  try {
    return globals.courses[_longName.toLowerCase()]['namn'];
  } catch (e) {
    try {
      return globals.courses[_shortName.toLowerCase()]['namn'];
    } catch (e) {
      return name;
    }
  }
}

gimmeIcon(chrs) { //TODO: replace switch with an array
  var _iconsplitthing;
  try {
    _iconsplitthing = chrs['title'].toString().split(",")[0].toLowerCase();
  } catch (e) {
    _iconsplitthing = chrs['title'].toString().toLowerCase();
  }
  var _ok = heck(_iconsplitthing.toString().toLowerCase());
  if (_ok == Icons.help) {
    try {
      _iconsplitthing = globals.reverseCourses[chrs['title'].toString().toLowerCase()][0]["kod"].toString().substring(0,3);
      _ok = heck(_iconsplitthing.toString().toLowerCase());
      return _ok;
    } catch (e) {return _ok;}
  } else {
    return _ok;
  }
}

heck(a) {
  switch (a) { //TODO: http://www.kunskapskrav.se/sok/ literally everything there
    case 'prr': //programmering
      return CIcons.code;
    case 'sve': //svenska
      return Icons.create;
    case 'sva': //svenska som andraspråk
      return Icons.create;
    case 'elr': //ellära
      return Icons.power;
    case 'ent': //entrepenörskap
      return Icons.attach_money;
    case 'mep': //mediaproduktion
      return Icons.add_to_queue;
    case 'mat': //mattematik
      return CIcons.calc;
    case 'nak': //naturkundskap
      return CIcons.leaf;
    case 'cad': //cad
      return CIcons.cube;
    case 'sjo': //grundläggande säkerhet sjöfart
      return Icons.lock;
    case 'far': //Lasthantering och passagerarsäkerhet sjöfart
      return Icons.card_travel;
    case 'mak': //Maskinbefäl sjöfart
      return Icons.build;
    case 'ver': //El- och verkstadsteknik
      return Icons.border_clear;
    case 'idr': //idrott
      return Icons.directions_run;
    case 'tek': //teknik
      return Icons.computer;
    case 'ark': //arkitektur
      return Icons.home;
    case 'hål': //????
      return Icons.filter_vintage;
    case 'dao': //dator och nätverk
      return Icons.router;
    case 'his': //historia
      return Icons.hourglass_empty;
    case 'fys': //fysik
      return Icons.device_hub;
    case 'kem': //kemi
      return CIcons.beaker;
    case 'sam': //sammhälleskundskap
      return Icons.person_pin;
    case 'psk': //psykologi
      return Icons.transfer_within_a_station;
    case 'weu': //webbutvekling
      return Icons.web;
    case 'eng': //engelska
      return Icons.create;
    case 'mus': //musik
      return Icons.music_note;
    case 'gyar': //gymnasiearbete
      return Icons.star;
    case 'gyarrl': //gymnasiearbete
      return Icons.star;
    case 'schema': //mentorstid
      return Icons.comment;
    case 'mentorstid': //mentorstid
      return Icons.comment;
    case 'mentor': //mentorstid
      return Icons.comment;
    case 'rel': //religion
      return CIcons.religious_christian;
    default:
      return Icons.help;
  }
}