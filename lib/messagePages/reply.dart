import 'package:flutter/material.dart';
import 'package:pingpong_schedule/app_localizations.dart';
import 'package:http/http.dart' as http;

Future<Null> replyDialog(context, name, key, userid) async {
  final textControllerr = new TextEditingController();
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(translated(context, "messages_replyto") + name),
        content: TextField(minLines: 5, maxLines: null, keyboardType: TextInputType.multiline, controller: textControllerr, decoration: InputDecoration(hintText: translated(context, "messages_message"))),
        actions: <Widget>[
          FlatButton(child: Text(translated(context, "messages_cancel")), onPressed: (){
            textControllerr.clear();
            Navigator.pop(context);
            return;
          }),
          FlatButton(child: Text(translated(context, "messages_reply")), onPressed: () async {
            if (textControllerr.text != "" && textControllerr.text != null) {
              String owo = textControllerr.text;
              Map<String, String> heck = Map();
              heck["recipient"] = userid;
              heck["message"] = owo;
              heck["apiKey"] = key;
              await http.post(
                "https://hjarntorget.goteborg.se/api/pim/send",
                body: heck,
              );
              textControllerr.clear();
              Navigator.pop(context);
              return;
            }
          },
          )],
      );
    },
    barrierDismissible: false
  );
}