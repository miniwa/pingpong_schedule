import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pingpong_schedule/styles.dart';
import 'package:http/http.dart' as http;
import 'package:pingpong_schedule/loading.dart';
import 'package:pingpong_schedule/globals.dart' as globals;
import 'dart:convert' as convert;

class Profile extends StatefulWidget {
  final String userid;
  Profile(this.userid, {Key key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  String _key = globals.prefs.getString('apiKey') ?? null;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: http.get("https://hjarntorget.goteborg.se/api/core/user-info?id="+this.widget.userid+"&language="+globals.lang+"&apiKey="+_key),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Animate.fadeSwitcher(Text("uh oh sphagettio's"));
            } else {
              var _snapshot = convert.jsonDecode(snapshot.data.body);
              return Animate.fadeSwitcher(
                SingleChildScrollView(
                  child: ConstrainedBox(
                    constraints: BoxConstraints(minHeight: 1),
                      child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                        CircleAvatar(backgroundImage: CachedNetworkImageProvider("https://hjarntorget.goteborg.se"+_snapshot["user"]["imagePath"]), radius: 32),
                        SizedBox(height: 8),
                        Text(_snapshot["user"]["firstName"] + " " + _snapshot["user"]["lastName"], style: TextStyle(fontSize: 32)),
                        SizedBox(height: 8),
                        Divider(),
                        SizedBox(height: 8),
                        for(var item in _snapshot["fields"] ) SelectableText.rich(TextSpan(children: <TextSpan>[
                          TextSpan(text: item["name"] + "\n", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                          TextSpan(text: item["value"] + "\n", style: TextStyle(fontSize: 12))
                        ]), textAlign: TextAlign.center,)
                    ],),
                  ),
                )
              );
            }
          } else {
            return Animate.fadeSwitcher(pingpongLoader(context));
          }
        },
      );
  }
}