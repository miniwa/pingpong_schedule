import 'package:flutter/material.dart';
import 'package:pingpong_schedule/app_localizations.dart';
import 'package:pingpong_schedule/styles.dart';
import 'package:http/http.dart' as http;
import 'package:pingpong_schedule/loading.dart';
import 'package:pingpong_schedule/globals.dart' as globals;
import 'dart:convert' as convert;

class AllMsg extends StatefulWidget {
  final String userid;
  final String name;
  AllMsg(this.userid, this.name, {Key key}) : super(key: key);

  @override
  _AllMsgState createState() => _AllMsgState();
}

class _AllMsgState extends State<AllMsg> {
  String _key = globals.prefs.getString('apiKey') ?? null;
  Future future;
  final textController = TextEditingController();

  @override
  void initState() { 
    super.initState();
    future = http.get(
      "https://hjarntorget.goteborg.se/api/pim/conversation-with?otherParticipantId=" +
      this.widget.userid +
      "&apiKey=" +
      _key);
  }

  @override
  void dispose() { 
    textController.dispose();
    super.dispose();
  }

  bool isYou(var item) {
    return item["sender"]["id"] != this.widget.userid;
  }

  void sendMessage() async {
    if (textController.text != "" && textController.text != null) {
      String owo = textController.text;
      textController.clear();
      Map<String, String> heck = Map();
      heck["recipient"] = this.widget.userid;
      heck["message"] = owo;
      heck["apiKey"] = _key;
      await http.post(
        "https://hjarntorget.goteborg.se/api/pim/send",
        body: heck,
      );
      setState(() {
        future = http.get(
      "https://hjarntorget.goteborg.se/api/pim/conversation-with?otherParticipantId=" +
      this.widget.userid +
      "&apiKey=" +
      _key);
      });
    }
  }

  Widget messageBox(bool useable) {
    return Container(width: MediaQuery.of(context).size.width, height: 48, padding: EdgeInsets.symmetric(horizontal: 32, vertical: 12), color: Colors.purple, child: Row(children: <Widget>[
        Expanded(child: TextField(controller: textController, decoration: InputDecoration(hintText: translated(context, "messages_message")),)),
        Padding(padding: EdgeInsets.fromLTRB(8, 0, 0, 0),child: GestureDetector(onTap: useable == true ? (){sendMessage();} : null, child: Icon(Icons.send)))
      ],));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(this.widget.name)),
      body: Container(
        child: FutureBuilder(
            future: future,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  return Animate.fadeSwitcher(Text("uh oh sphagettio's"));
                } else {
                  var _snapshot = convert.jsonDecode(snapshot.data.body);
                  return Animate.fadeSwitcher(Column(
                    children: <Widget>[
                      Expanded(
                        child: SingleChildScrollView(
                          reverse: true,
                          child: ConstrainedBox(
                            constraints: BoxConstraints(minHeight: 1),
                            child: Column(
                              children: <Widget>[
                                for (var item in _snapshot)
                                  Container(
                                    decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(20)), color: isYou(item) ? Colors.purple : Colors.grey[200]),
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.all(16),
                                    margin: EdgeInsets.fromLTRB(isYou(item) ? 32 : 16, 16, isYou(item) ? 16 : 32, 16),
                                    child: Column(
                                      children: <Widget>[
                                        ConstrainedBox(constraints: BoxConstraints(minWidth: double.infinity), child: SelectableText(item["text"], style: isYou(item) ? TextStyle(color: Colors.white) : TextStyle(color: Colors.black))),
                                        item["attachments"].length > 0 ? Text("Bifogad fil", style: isYou(item) ? TextStyle(color: Colors.white) : TextStyle(color: Colors.black)) : SizedBox(),
                                      ],
                                    )),
                              ],
                            ),
                          ),
                        ),
                      ),
                      messageBox(true),
                    ],
                  ));
                }
              } else {
                return Animate.fadeSwitcher(Column(children: <Widget>[
                  Expanded(child: Center(child: pingpongLoader(context))),
                  messageBox(false),
                ]));
              }
            }),
      ),
    );
  }
}
