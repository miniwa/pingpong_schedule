import 'package:flutter/material.dart';
import 'package:pingpong_schedule/app_localizations.dart';
import 'package:pingpong_schedule/styles.dart';
import 'package:pingpong_schedule/globals.dart' as globals;
import 'dart:convert' as convert;

class Tests extends StatefulWidget {
  Tests({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _TestsState createState() => _TestsState();
}

class _TestsState extends State<Tests> {
  List _tests = [0];
  List _testsCards = [0];
  bool _coursenames = true;
  bool _showfulname;

  gimmeTimes(int start, int stop) {
    var beg = new DateTime.fromMillisecondsSinceEpoch(start);
    var end = new DateTime.fromMillisecondsSinceEpoch(stop);
    return (
        beg.month.toString().padLeft(2, "0") +
        "/" +
        beg.day.toString().padLeft(2, "0") +
        "\n" +
        beg.hour.toString().padLeft(2, "0") +
        ":" +
        beg.minute.toString().padLeft(2, "0") +
        "-" +
        end.hour.toString().padLeft(2, "0") +
        ":" +
        end.minute.toString().padLeft(2, "0"));
  }

  loadStuff() async {
    try {
      setState(() {
        _tests = convert.jsonDecode(globals.prefs.getString("tests")) ?? [0];
        _testsCards = convert.jsonDecode(globals.prefs.getString("testsCards")) ?? [0];
      });
    } on NoSuchMethodError {
      setState(() {
        _tests = [0];
        _testsCards = [0];
      });
    }
    setState(() {
      _showfulname = globals.prefs.getBool('showfulname') ?? false;
      _coursenames = globals.prefs.getBool('coursenames') ?? true;
    });
  }

  initState() {
    super.initState();
    loadStuff();
  }
  //end bullshit portion

  teacherNames(List names) {
    String plumee = "";
    for (String name in names) {
      try {
        if (_showfulname) {
          plumee += name.toString().split('(')[0];
        } else {
          plumee += name.toString().split('(')[1].split(')')[0]+" ";
        }
      } catch(e) {
        plumee += name.toString()+" ";
      }
    }
    return plumee;
  }

  errorWidget() {
    return Container(
      child: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.check, size: 100),
          Text(translated(context, 'tests_none'), textAlign: TextAlign.center, style: TextStyle(fontSize: 32)),
          Text(translated(context, 'gesturehelp_scheddouble'), textAlign: TextAlign.center, style: TextStyle(fontSize: 24)),
        ],
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    if (_showfulname == null) {
      return Animate.fadeSwitcher(Container());
    } else {
    return Animate.fadeSwitcher(Container(
      child: (_testsCards.length - 1 == 0) ? Animate.fadeSwitcher(errorWidget()) : Animate.fadeSwitcher(ListView.separated(
        itemCount: _testsCards.length,
        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
        itemBuilder: (BuildContext context, int index) {
          if (index == 0) {
            return Text(
              translated(context, 'bottomnavbar_tests'),
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 48, fontWeight: FontWeight.w300));
          }
          var _item = convert.jsonDecode(_testsCards[index]);
          return Dismissible(
            key: Key(_item.toString()),
            onDismissed: (direction) async {
              var a = _item['startDate']['ts'];
              var b = convert.jsonEncode(_item);
              setState(() {
                _tests.remove(a);
                _testsCards.remove(b);
              });
              globals.prefs.setString("tests", convert.jsonEncode(_tests).toString());
              globals.prefs.setString("testsCards", convert.jsonEncode(_testsCards).toString());
            },
            child: Card(
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: Icon(
                        gimmeIcon(_item),
                        size: 48),
                    title: Text(_coursenames ? courseName(_item['title']) : _item['title'],
                        style: TextStyle(
                            color: _tests.contains(_item['startDate']['ts'])
                                ? Colors.red
                                : null)),
                    subtitle: Text(_item['location'] + " " + teacherNames(_item['bookedTeacherNames'])),
                    trailing: Text(gimmeTimes(_item['startDate']['ts'] + _item['startDate']['timezoneOffsetMinutes']*60,
                        _item['endDate']['ts'] + _item['endDate']['timezoneOffsetMinutes']*60), textAlign: TextAlign.center),
                  ),
                ],
                mainAxisSize: MainAxisSize.min,
              ),
            ),
          );
        },
        separatorBuilder: (BuildContext context, int index) {
          if (index == 0) {
            return SizedBox(
              height: 10,
            );
          } else {
            return Divider(color: Colors.grey, height: 10);
          }
        },
      )),
    ));
  }}
}
