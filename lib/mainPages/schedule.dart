import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:pingpong_schedule/styles.dart';
import 'package:http/http.dart' as http;
import 'package:pingpong_schedule/keyPages/login.dart';
import 'package:pingpong_schedule/app_localizations.dart';
import 'package:pingpong_schedule/loading.dart';
import 'package:pingpong_schedule/globals.dart' as globals;
import 'dart:convert' as convert;
import 'package:intl/intl.dart';
import 'dart:io';

class Schedule extends StatefulWidget {
  Schedule({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ScheduleState createState() => _ScheduleState();
}

class _ScheduleState extends State<Schedule> {
  Future _future;
  int _days = 0;
  bool _garfield = false;
  bool _showfulname = false;
  bool _showfab = true;
  bool _coursenames;
  bool _hawawa = false;
  List _tests = [0];
  List _testsCards = [0];
  BuildContext _fuckingContext;
  var _scrollDir = ScrollDirection.forward;

  gimmeTimes(int start, int stop) {
    var beg = new DateTime.fromMillisecondsSinceEpoch(start);
    var end = new DateTime.fromMillisecondsSinceEpoch(stop);
    return (beg.hour.toString().padLeft(2, "0") +
        ":" +
        beg.minute.toString().padLeft(2, "0") +
        "-" +
        end.hour.toString().padLeft(2, "0") +
        ":" +
        end.minute.toString().padLeft(2, "0"));
  }

  Future<String> apiKey() async {
    String _key = globals.prefs.getString('apiKey') ?? null;
    if (_key == null) {
      String _return = await Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => Login(allowBack: false)));
      if (_return == null) {
        //fuck this shit, the user is an idiot
        exit(0);
        return "lol";
      } else {
        return _return;
      }
    } else {
      return _key;
    }
  }

  Future<String> userId(String piKey) async {
    String _id = globals.prefs.getString('userId') ?? null;
    if (_id == null) {
      var now = DateTime.fromMillisecondsSinceEpoch(
          new DateTime.now().millisecondsSinceEpoch);
      var day = now.day.toString().padLeft(2, "0");
      var mon = now.month.toString().padLeft(2, "0");
      var yer = now.year.toString();
      var dmy = yer + "-" + mon + "-" + day;
      var dumb = await http.get(
          "https://hjarntorget.goteborg.se/api/schema/lessons?forUser=201150_goteborgsstad&startDateIso=" +
              dmy +
              "&endDateIso=" +
              dmy +
              "&apiKey=" +
              piKey);
      if (dumb.body.substring(dumb.body.length - 13) == "goteborgsstad") {
        globals.prefs.setString('userId',
            '201150_goteborgsstad'); //either the ONE PERSON I USE AS A TEST uses my app... or there is an error. lets assume the first.
        return '201150_goteborgsstad';
      } else {
        globals.prefs.setString('userId',
            dumb.body.split("ID: ")[1].trim());
        return dumb.body.split("ID: ")[1].trim();
        
      }
    } else {
      return _id;
    }
  }

  String getDate(int a) {
    var now = DateTime.fromMillisecondsSinceEpoch(
        new DateTime.now().millisecondsSinceEpoch + (_days * 86400000));
    if (_garfield && (now.weekday == 6 || now.weekday == 7)) {
      if (now.weekday == 6) {
        _days += 2;
      } else {
        _days += 1;
      }
      now = DateTime.fromMillisecondsSinceEpoch(
          new DateTime.now().millisecondsSinceEpoch + (_days * 86400000));
    }
    var day = now.day.toString().padLeft(2, "0");
    var mon = now.month.toString().padLeft(2, "0");
    var yer;
    switch (now.weekday) {
      case 1:
        yer = translated(context, 'monday');
        break;
      case 2:
        yer = translated(context, 'tuesday');
        break;
      case 3:
        yer = translated(context, 'wednesday');
        break;
      case 4:
        yer = translated(context, 'thursday');
        break;
      case 5:
        yer = translated(context, 'friday');
        break;
      case 6:
        yer = translated(context, 'saturday');
        break;
      case 7:
        yer = translated(context, 'sunday');
        break;
    }
    if (a == 1) {
      return now.weekday.toString();
    } else if (a == 2) {
      return ((int.parse(DateFormat("D").format(now)) - 6 + 10)/7).round().toString();
    } else {
      return yer + " " + mon + "-" + day;
    }
  }

  errorWidget(String errar) {
    return Container(
        child: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.warning, size: 100),
          Text(translated(context, 'error_uhoh'),
              textAlign: TextAlign.center, style: TextStyle(fontSize: 32)),
          Text(translated(context, errar),
              textAlign: TextAlign.center, style: TextStyle(fontSize: 24)),
          SizedBox(height: 10),
          RaisedButton(
            child: Text(translated(context, 'error_retry'), style: TextStyle(color: Colors.white)),
            color: Colors.purple,
            onPressed: () {
              setState(() {
                _future = getFuture();
              });
            },
          ),
        ],
      ),
    ));
  }

  //begin bullshit portion
  Future getFuture() async {
    String piKey = await apiKey();
    String useId = await userId(piKey);
    var now = DateTime.fromMillisecondsSinceEpoch(
        new DateTime.now().millisecondsSinceEpoch + (_days * 86400000));
    if (_garfield && (now.weekday == 6 || now.weekday == 7)) {
      if (now.weekday == 6) {
        _days += 2;
      } else {
        _days += 1;
      }
      now = DateTime.fromMillisecondsSinceEpoch(
          new DateTime.now().millisecondsSinceEpoch + (_days * 86400000));
    }
    var day = now.day.toString().padLeft(2, "0");
    var mon = now.month.toString().padLeft(2, "0");
    var yer = now.year.toString();
    var dmy = yer + "-" + mon + "-" + day;
    return http.get(
        "https://hjarntorget.goteborg.se/api/schema/lessons?forUser=" +
            useId.trim() +
            "&startDateIso=" +
            dmy +
            "&endDateIso=" +
            dmy +
            "&apiKey=" +
            piKey);
  }

  loadStuff() {
    try {
      _tests = convert.jsonDecode(globals.prefs.getString("tests")) ?? [0];
      _testsCards = convert.jsonDecode(globals.prefs.getString("testsCards")) ?? [0];
    } on NoSuchMethodError {
      _tests = [0];
      _testsCards = [0];
    }
    _garfield = globals.prefs.getBool('garfield') ?? true; //good enough for now i guess
    _showfulname = globals.prefs.getBool('showfulname') ?? false;
    _coursenames = globals.prefs.getBool('coursenames') ?? true;
    _hawawa = globals.prefs.getBool("darktheme") ?? false;
  }

  initState() {
    super.initState();
    loadStuff();
  }
  //end bullshit portion

  teacherNames(List names) {
    String plumee = "";
    for (String name in names) {
      try {
        if (_showfulname) {
          plumee += name.toString().split('(')[0];
        } else {
          plumee += name.toString().split('(')[1].split(')')[0] + " ";
        }
      } catch (e) {
        plumee += name.toString() + " ";
      }
    }
    return plumee;
  }

  changeDate() async {
    DateTime now = DateTime.now();
    DateTime selectedDate = await showDatePicker(
      context: context,
      initialDate: now,
      initialDatePickerMode: DatePickerMode.day,
      firstDate: DateTime(now.year - 4),
      lastDate: DateTime(now.year + 4),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: MediaQuery.of(context).platformBrightness.toString() == Brightness.dark.toString() || _hawawa
              ? Themes().darkTheme
              : Themes().lightTheme,
          child: child,
        );
      },
    );
    if (selectedDate == null) {
      return;
    }
    Duration watt;
    watt = selectedDate.difference(DateTime(now.year, now.month, now.day));
    if (_garfield && (selectedDate.weekday == 6 || selectedDate.weekday == 7)) {
      Scaffold.of(_fuckingContext ?? context).showSnackBar(SnackBar(
          content: Text(translated(context, "schedule_skiptomonday"))));
    }
    setState(() {
      _days = watt.inDays;
      _future = getFuture();
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_future == null) {
      WidgetsBinding.instance.addPostFrameCallback((_) => {
        setState(() {_future = getFuture();})
      });
      return Animate.fadeSwitcher(Container());
    } else {
      return Animate.fadeSwitcher(Scaffold(
        floatingActionButton: !_showfab
            ? null
            : FloatingActionButton(
                child: Icon(Icons.date_range),
                backgroundColor: Colors.purple,
                onPressed: changeDate,
                tooltip: translated(context, "schedule_gotodate"),
              ),
        body: Container(
          child: FutureBuilder(
            future: _future,
            builder: (context, snapshot) {
              _fuckingContext = context;
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  return Animate.fadeSwitcher(errorWidget('error_wrong'));
                } else {
                  _showfab = true;
                  try {
                    convert.jsonDecode(snapshot.data.body);
                  } on FormatException {
                    return Animate.fadeSwitcher(errorWidget('error_hjarn'));
                    //return errorWidget('error_hjarn');
                  }
                  return Animate.fadeSwitcher(GestureDetector(
                    onLongPress: () {
                      //long press or double tap???
                      var now = DateTime.fromMillisecondsSinceEpoch(
                          new DateTime.now().millisecondsSinceEpoch);
                      if ((_garfield == false && _days != 0) ||
                          (_garfield &&
                              (((now.weekday != 6) &&
                                      (now.weekday != 7) &&
                                      _days != 0) ||
                                  (now.weekday == 6 && _days != 2) ||
                                  (now.weekday == 7 && _days != 1)))) {
                        setState(() {
                          _days = 0;
                          _future = getFuture();
                        });
                      }
                    },
                    onHorizontalDragEnd: (dragEndDetails) {
                      setState(() {
                        if (_garfield) {
                          if (dragEndDetails.primaryVelocity < 0) {
                            if (getDate(1) == "5") {
                              _days += 3;
                            } else {
                              _days++;
                            }
                          } else {
                            if (getDate(1) == "1") {
                              _days -= 3;
                            } else {
                              _days--;
                            }
                          }
                        } else {
                          dragEndDetails.primaryVelocity < 0
                              ? _days++
                              : _days--;
                        }
                        _future = getFuture();
                      });
                    },
                    child: NotificationListener<ScrollNotification>(
                      onNotification: (scrollNotification) {
                        if (scrollNotification is UserScrollNotification) {
                          if (scrollNotification.metrics.maxScrollExtent != 0) {
                            setState(() {
                              _scrollDir = scrollNotification.direction;
                            });
                          }
                          if (_scrollDir == ScrollDirection.forward) {
                            setState(() {
                              _showfab = true;
                            });
                          }
                        }
                        if (scrollNotification is OverscrollNotification) {
                          if (_scrollDir == ScrollDirection.reverse) {
                            setState(() {
                              _showfab = false;
                            });
                          }
                        }
                        return true;
                      },
                      child: convert.jsonDecode(snapshot.data.body).length == 0 ? ListView(padding: EdgeInsets.all(10), children: <Widget>[
                              Text(getDate(0),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 48, fontWeight: FontWeight.w300)),
                              SizedBox(height: 10),
                              Text("\n" + translated(context, "week") + " " + getDate(2), textAlign: TextAlign.center, style: TextStyle(color: Colors.grey))
                            ])
                        : ListView.separated(
                        itemCount:
                            convert.jsonDecode(snapshot.data.body).length + 1,
                        padding: EdgeInsets.all(10),
                        itemBuilder: (BuildContext context, int index) {
                          if (index == 0) {
                            return Text(getDate(0),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 48, fontWeight: FontWeight.w300));
                          }
                          var _item =
                              convert.jsonDecode(snapshot.data.body)[index - 1];
                          return GestureDetector(
                              onDoubleTap: () async {
                                var a = _item['startDate']['ts'];
                                var b = convert.jsonEncode(_item);
                                setState(() {
                                  if (_tests.contains(a)) {
                                    _tests.remove(a);
                                    _testsCards.remove(b);
                                  } else {
                                    _tests.add(a);
                                    _testsCards.add(b);
                                  }
                                });
                                globals.prefs.setString("tests",
                                    convert.jsonEncode(_tests).toString());
                                globals.prefs.setString("testsCards",
                                    convert.jsonEncode(_testsCards).toString());
                              },
                              child: Column(
                                children: <Widget>[
                                  Card(
                                      child: Column(
                                    children: <Widget>[
                                      ListTile(
                                        leading: Container(
                                          width: 48,
                                          height: 48,
                                          child: Icon(
                                              gimmeIcon(_item),
                                              size: 48),
                                        ),
                                        title: Text(
                                            _coursenames
                                                ? courseName(_item['title'])
                                                : _item['title'],
                                            style: TextStyle(
                                                color: _tests.contains(
                                                        _item['startDate']['ts'])
                                                    ? Colors.red
                                                    : null)),
                                        subtitle: Text(_item['location'] +
                                            " " +
                                            teacherNames(
                                                _item['bookedTeacherNames'])),
                                        trailing: Text(
                                            gimmeTimes(
                                                _item['startDate']['ts'] +
                                                    _item['startDate'][
                                                            'timezoneOffsetMinutes'] *
                                                        60,
                                                _item['endDate']['ts'] +
                                                    _item['endDate'][
                                                            'timezoneOffsetMinutes'] *
                                                        60),
                                            textAlign: TextAlign.center),
                                      ),
                                    ],
                                    mainAxisSize: MainAxisSize.min,
                                  )),
                                  convert.jsonDecode(snapshot.data.body).length == index ? Text("\n" + translated(context, "week") + " " + getDate(2), style: TextStyle(color: Colors.grey),) : SizedBox(height: 0)
                                ],
                              ));
                        },
                        separatorBuilder: (BuildContext context, int index) {
                          if (index == 0) {
                            return SizedBox(
                              height: 10,
                            );
                          } else {
                            return Divider(color: Colors.grey, height: 10);
                          }
                        },
                      ),
                    ),
                  ));
                }
              } else {
                return Animate.fadeSwitcher(
                    Center(child: pingpongLoader(context)));
              }
            },
          ),
        ),
      ));
    }
  }
}
