import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pingpong_schedule/app_localizations.dart';
import 'package:pingpong_schedule/messagePages/allmsg.dart';
import 'package:pingpong_schedule/messagePages/profile.dart';
import 'package:pingpong_schedule/messagePages/reply.dart';
import 'package:pingpong_schedule/styles.dart';
import 'package:http/http.dart' as http;
import 'package:pingpong_schedule/loading.dart';
import 'package:pingpong_schedule/globals.dart' as globals;
import 'dart:convert' as convert;

class Messages extends StatefulWidget {
  Messages({Key key}) : super(key: key);

  @override
  MessagesState createState() => MessagesState();
}

class MessagesState extends State<Messages>
    with SingleTickerProviderStateMixin {
  
  String _key = globals.prefs.getString('apiKey') ?? null;

  List<Tab> tabWidgets = <Tab>[
    Tab(text: 'Pim'),
    Tab(text: 'Information'),
  ];

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: tabWidgets.length);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Animate.fadeSwitcher(Scaffold(
      appBar: TabBar(tabs: tabWidgets, controller: _tabController,),
      body: Container(
      child: 
        TabBarView(
          controller: _tabController,
          children: tabWidgets.map((Tab tab) {
            bool _isPim = (tab.text == "Pim" ? true : false);
            if (_key == null) {
              return Text("how did u even get here lmao");
            } else {
              return FutureBuilder(
                future: http.get("https://hjarntorget.goteborg.se/api/" + (_isPim ? "pim/conversations?apiKey=" : "information/messages-by-date-desc?messageStatus=ALL&offset=0&limit=100&language=en&apiKey=") + _key),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasError) {
                      return Animate.fadeSwitcher(Text("uh oh sphagettio's"));
                    } else {
                      try {
                        var _snapshot = convert.jsonDecode(snapshot.data.body);
                        return Animate.fadeSwitcher(
                          ListView.builder(
                            padding: EdgeInsets.all(10),
                            itemCount: _snapshot.length,
                            itemBuilder: (BuildContext context, int index) {
                              String _title = _isPim ? _snapshot[index]["otherParty"]["firstName"] + " " + _snapshot[index]["otherParty"]["lastName"] : _snapshot[index]["creator"]["firstName"] + " " + _snapshot[index]["creator"]["lastName"];
                              String _subtitle = _isPim ? DateTime.fromMillisecondsSinceEpoch(_snapshot[index]["latestDate"]["ts"]).add(Duration(minutes: _snapshot[index]["latestDate"]["timezoneOffsetMinutes"])).toString() : _snapshot[index]["title"];
                              String _text = _isPim ? _snapshot[index]["latestText"] : _snapshot[index]["body"];
                              String _img = _isPim ? "https://hjarntorget.goteborg.se"+_snapshot[index]["otherParty"]["imagePath"] : "https://hjarntorget.goteborg.se"+_snapshot[index]["creator"]["imagePath"];
                              String _id = _isPim ? _snapshot[index]["otherParty"]["id"] : _snapshot[index]["creator"]["id"];

                              return ExpansionTile(
                                key: PageStorageKey<String>(index.toString()),
                                title: Text(_title),
                                subtitle: Text(_subtitle),
                                children: <Widget>[
                                  Text(_text),
                                  ButtonBar(
                                    children: <Widget>[
                                      FlatButton(child: Text(translated(context, "messages_profile")), onPressed: (){showDialog(context: context, child: AlertDialog(content: Profile(_id)));} ),
                                      _isPim ? FlatButton(child: Text(translated(context, "messages_reply")), onPressed: (){replyDialog(context, _title, _key, _id);} ) : null,
                                      _isPim ? RaisedButton(child: Text(translated(context, "messages_moremessages")), onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => AllMsg(_id, _title)));} ) : RaisedButton(child: Text(translated(context, "messages_attatchment")), onPressed: _snapshot[index]["attachments"].Length > 0 ? (){showDialog(context: context, child: AlertDialog(content: Text(translated(context, "messages_nextupdate"))));} : null ),
                                    ],
                                  )
                                ],
                                leading: _img == "https://hjarntorget.goteborg.se/pp/lookAndFeel/skins/hjarntorget/icons/monalisa_large.png"
                                  ? CircleAvatar(child: Text(_title.substring(0,2)))
                                  : CircleAvatar(backgroundImage: CachedNetworkImageProvider(_img)),
                              );
                            },
                          )
                        );
                      } catch (e) {
                        return Animate.fadeSwitcher(Text("uh oh sphagettio's"));
                      }
                    }
                  } else {
                    return Animate.fadeSwitcher(Center(child: pingpongLoader(context)));
                  }
                }
              );
            }
          }).toList())
    )));
  }
}
