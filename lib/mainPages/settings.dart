import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pingpong_schedule/keyPages/login.dart';
import 'package:pingpong_schedule/infoPages/about.dart';
import 'package:pingpong_schedule/infoPages/ossLicenses.dart';
import 'package:pingpong_schedule/infoPages/help.dart';
import 'package:pingpong_schedule/app_localizations.dart';
import 'package:pingpong_schedule/styles.dart';
import 'package:pingpong_schedule/globals.dart' as globals;
import 'package:share/share.dart';
import 'package:package_info/package_info.dart';

class Settings extends StatefulWidget {
  Settings({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  final _formKey = GlobalKey<FormState>();
  String _apiKey = "";
  PackageInfo packageInfo;
  bool _garfield = true;
  bool _showfulname = false;
  bool _hawawa = false;
  bool _grey = false;
  bool _coursenames = true;
  final key = new GlobalKey<ScaffoldState>();

  loadSettings() async {
    packageInfo = await PackageInfo.fromPlatform();
    setState(() {
      _apiKey = globals.prefs.getString("apiKey");
      _hawawa = globals.prefs.getBool("darktheme") ?? false;
      _grey = globals.prefs.getBool("greybg") ?? false;
      _garfield = globals.prefs.getBool("garfield") ?? true;
      _showfulname = globals.prefs.getBool("showfulname") ?? false;
      _coursenames = globals.prefs.getBool("coursenames") ?? true;
    });
  }

  @override
  void initState() {
    loadSettings();
    super.initState();
  }

  nav(var a) {
    return () {
      Navigator.push(
          context, MaterialPageRoute(builder: (BuildContext context) => a));
    };
  }

  ListTile coolTile(Icon icon, Text text, onTapp) {
    return ListTile(leading: icon, title: text, onTap: onTapp);
  }

  CheckboxListTile settingTile(Icon icon, Text text, onChanged, value) {
    return CheckboxListTile(
      activeColor: Colors
          .purple, //this is about a million times easier than figuring out what im missing in my main.dart
      title: text,
      secondary: icon,
      value: value,
      onChanged: onChanged
    );
  }

  bool systemDarkOn() {
    return MediaQuery.of(context).platformBrightness.toString() ==
            Brightness.dark.toString()
        ? true
        : false;
  }

  @override
  Widget build(BuildContext context) {
    if (_apiKey == "" && packageInfo == null) {
      return Animate.fadeSwitcher(Container());
    } else {
      return Animate.fadeSwitcher(Scaffold(
        key: key,
        body: Container(
          child: ListView(padding: EdgeInsets.all(10), children: <Widget>[
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  GestureDetector(
                    child: coolTile(
                      Icon(Icons.vpn_key),
                      Text(translated(context, "settings_eapia")),
                      nav(Login(allowBack: true))),
                    onLongPress: () {
                      Clipboard.setData(new ClipboardData(text: _apiKey));
                      key.currentState.showSnackBar(SnackBar(content: Text(translated(context, "settings_copyapikey"))));
                    },
                  ),
                  settingTile(
                    Icon(Icons.calendar_today),
                    Text(translated(context, "settings_skip")),
                    (value) async {
                      setState(() {
                        globals.prefs.setBool("garfield", value);
                        _garfield = value;
                      });
                    },
                    _garfield
                  ),
                  settingTile(
                    Icon(Icons.face),
                    Text(translated(context, "settings_fullname")),
                    (value) async {
                      setState(() {
                        globals.prefs.setBool("showfulname", value);
                        _showfulname = value;
                      });
                    },
                    _showfulname
                  ),
                  settingTile(
                    Icon(Icons.short_text),
                    Text(translated(context, "settings_coursename")),
                    (value) async {
                      setState(() {
                        globals.prefs.setBool("coursenames", value);
                        _coursenames = value;
                      });
                    },
                    _coursenames
                  ),
                  settingTile(
                    Icon(Icons.brightness_medium, color: systemDarkOn() ? Colors.white : null),
                    Text(translated(context, "settings_darkmode"), style: TextStyle(color: systemDarkOn() ? Colors.white : null)),
                    systemDarkOn()
                        ? null
                        : (value) async {
                            setState(() {
                              _hawawa = value;
                              globals.darkMode = _hawawa;
                              globals.prefs.setBool("darktheme", value);
                              themeBloc.changeTheme(value);
                            });
                          },
                    systemDarkOn() ? true : _hawawa,
                  ),
                  (!systemDarkOn() && !_hawawa) ? Animate.fadeSwitcher(SizedBox(height:0)) : Animate.fadeSwitcher(settingTile(
                    Icon(Icons.gradient),
                    Text(translated(context, "settings_greymode")),
                    (!_hawawa && !systemDarkOn()) ? null :
                    (value) async {
                            setState(() {
                              _grey = value;
                              globals.greyBg = _grey;
                              globals.prefs.setBool("greybg", value);
                              greyBloc.changeTheme(value);
                            });
                          },
                    _grey,
                  )),
                  Divider(color: Colors.grey, height: 10),
                  coolTile(Icon(Icons.info),
                      Text(translated(context, "settings_about")), nav(About())),
                  coolTile(
                      Icon(Icons.help),
                      Text(translated(context, "gesturehelp_title")),
                      nav(Help())),
                  coolTile(Icon(Icons.share),
                      Text(translated(context, "settings_share")), () {
                    Share.share(translated(context, "settings_sharestr"));
                  }),
                  coolTile(
                      Icon(Icons.people),
                      Text(translated(context, "opensourcelicenses")),
                      nav(OssLicenses())),
                  Divider(color: Colors.grey, height: 10),
                  ListTile(
                      leading: Icon(Icons.update, color: Colors.grey),
                      title: Text(
                        translated(context, "settings_version") +
                            " " +
                            packageInfo.version,
                        style: TextStyle(color: Colors.grey),
                      )),
                ],
              ),
            ),
          ]),
        ),
      ));
    }
  }
}
