import 'package:flutter/material.dart';
import 'package:pingpong_schedule/app.dart';
import 'package:pingpong_schedule/styles.dart';
import 'package:pingpong_schedule/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pingpong_schedule/globals.dart' as globals;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  globals.prefs = await SharedPreferences.getInstance();
  bool them = globals.prefs.getBool("darktheme") ?? false;
  bool greyBg = globals.prefs.getBool("greybg") ?? false;
  runApp(MyApp(useDark: them, useGrey: greyBg));
} 

class MyApp extends StatelessWidget {
  final bool useDark;
  final bool useGrey;
  MyApp({Key key, @required this.useDark, @required this.useGrey}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: themeBloc.darkThemeEnabled,
      initialData: useDark ? true : false,
      builder: (context, snapshot) {
        var _darkOn = snapshot.data;
        return StreamBuilder(
          stream: greyBloc.greyThemeEnabled,
          initialData: useGrey ? true : false,
          builder: (context, snapshot) {
            return MaterialApp(
              title: 'TTSchedule',
              theme: _darkOn ? (snapshot.data ? Themes().greyTheme : Themes().darkTheme) : Themes().lightTheme,
              darkTheme: snapshot.data ? Themes().greyTheme : Themes().darkTheme,
              home: Application(),
              supportedLocales: [
                Locale('en', 'US'),
                Locale('sv', 'SE'),
              ],
              localizationsDelegates: [
                AppLocalizations.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
              ],
              localeResolutionCallback: (locale, supportedLocales) {
                for (var supportedLocale in supportedLocales) {
                  if (supportedLocale.languageCode == locale.languageCode &&
                      supportedLocale.countryCode == locale.countryCode) {
                    globals.lang = supportedLocale.toLanguageTag().substring(0,2);
                    return supportedLocale;
                  }
                }
                globals.lang = supportedLocales.first.toLanguageTag().substring(0,2);
                return supportedLocales.first;
              },
            );
          }
        );
      }
    );
  }
}