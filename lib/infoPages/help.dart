import 'package:flutter/material.dart';
import 'package:pingpong_schedule/app_localizations.dart';


class Help extends StatefulWidget {
  Help({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HelpState createState() => _HelpState();
}

class _HelpState extends State<Help> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(translated(context, "gesturehelp_appbar"))),
      body: Container(
        child: ListView(
          padding: EdgeInsets.all(10),
          children: <Widget>[
            Text(translated(context, "gesturehelp_title"), textAlign: TextAlign.center, style: TextStyle(fontSize: 32)),
            SizedBox(height: 30),
            Text(translated(context, "gesturehelp_sched"), textAlign: TextAlign.center, style: TextStyle(fontSize: 24)),
            Text(translated(context, "gesturehelp_schedswipe"), textAlign: TextAlign.center, style: TextStyle(fontSize: 16)),
            Text(translated(context, "gesturehelp_schedhold"), textAlign: TextAlign.center, style: TextStyle(fontSize: 16)),
            Text(translated(context, "gesturehelp_scheddouble"), textAlign: TextAlign.center, style: TextStyle(fontSize: 16)),
            Text(translated(context, "gesturehelp_schedcal"), textAlign: TextAlign.center, style: TextStyle(fontSize: 16)),
            SizedBox(height: 30),
            Text(translated(context, "gesturehelp_tests"), textAlign: TextAlign.center, style: TextStyle(fontSize: 24)),
            Text(translated(context, "gesturehelp_testsswipe"), textAlign: TextAlign.center, style: TextStyle(fontSize: 16)),
          ],
        ),
      ),
    );
  }
}
