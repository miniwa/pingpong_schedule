import 'package:flutter/material.dart';
import 'package:pingpong_schedule/app_localizations.dart';

class OssLicenses extends StatefulWidget {
  OssLicenses({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _OssLicensesState createState() => _OssLicensesState();
}

class _OssLicensesState extends State<OssLicenses> {
  @override
  Widget build(BuildContext context) {
    plumisacute(String title, String link, double size) {
      return Column(children: <Widget>[
        Text(title, textAlign: TextAlign.center, style: TextStyle(fontSize: size)),
        Text(link),
        Divider(color: Colors.grey, height: (size+6)),
      ],);
    }

    return Scaffold(
      appBar: AppBar(title: Text(translated(context, "opensourcelicenses"))),
      body: Container(
        child: ListView(
          padding: EdgeInsets.all(10),
          children: <Widget>[
            plumisacute('flutter', 'https://raw.githubusercontent.com/flutter/flutter/master/LICENSE', 24),
            plumisacute('flutter_webview_plugin', 'https://raw.githubusercontent.com/fluttercommunity/flutter_webview_plugin/master/LICENSE', 24),
            plumisacute('http', 'https://raw.githubusercontent.com/dart-lang/http/master/LICENSE', 24),
            plumisacute('shared_preferences', 'https://raw.githubusercontent.com/flutter/plugins/master/packages/shared_preferences/LICENSE', 24),
            plumisacute('share', 'https://raw.githubusercontent.com/flutter/plugins/master/packages/share/LICENSE', 24),
            plumisacute('package_info', 'https://raw.githubusercontent.com/flutter/plugins/master/packages/package_info/LICENSE', 24),
            plumisacute('flare_flutter', 'https://raw.githubusercontent.com/2d-inc/Flare-Flutter/master/LICENSE', 24),
            plumisacute('connectivity', 'https://raw.githubusercontent.com/flutter/plugins/master/packages/connectivity/connectivity/LICENSE', 24),
            plumisacute('cached_network_image', 'https://raw.githubusercontent.com/Baseflow/flutter_cached_network_image/master/LICENSE.md', 24),
            plumisacute('font awesome', 'https://fontawesome.com/license/free', 24),
            plumisacute('maki', 'https://github.com/mapbox/maki/blob/master/LICENSE.txt', 24),
          ],
        ),
      ),
    );
  }
}
