import 'package:flutter/material.dart';
import 'package:pingpong_schedule/app_localizations.dart';

class About extends StatefulWidget {
  About({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(translated(context, 'about_appbar'))),
      body: Container(
        child: ListView(
          padding: EdgeInsets.all(10),
          children: <Widget>[
            Text(translated(context, 'about_title'), textAlign: TextAlign.center, style: TextStyle(fontSize: 32)),
            Text(translated(context, 'about_by'), textAlign: TextAlign.center, style: TextStyle(fontSize: 24)),
            SizedBox(height: 30),
            Text(translated(context, 'about_blab')),
            SizedBox(height: 30),
            Text(translated(context, 'about_oss'), textAlign: TextAlign.center, style: TextStyle(fontSize: 24)),
            Text("https://gitlab.com/miniwa/pingpong_schedule", textAlign: TextAlign.center, style: TextStyle(fontSize: 16)),
          ],
        ),
      ),
    );
  }
}
