import 'package:flutter/material.dart';
import 'dart:async';
import 'package:pingpong_schedule/app_localizations.dart';
import 'package:pingpong_schedule/globals.dart' as globals;

class KeySettings extends StatefulWidget {
  KeySettings({Key key, this.title, this.allowBack}) : super(key: key);

  final String title;
  final bool allowBack;

  @override
  _KeySettingsState createState() => _KeySettingsState();
}

class _KeySettingsState extends State<KeySettings> {
  final _formKey = GlobalKey<FormState>();
  String _apiKey;
  
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(title: Text(translated(context, 'setmanual_appbar')), automaticallyImplyLeading: this.widget.allowBack ?? false),
        body: SafeArea(
        child: Form(
          key: _formKey,
          child: ListView(
            padding: EdgeInsets.all(10),
            children: <Widget>[
            Text(translated(context, 'setmanual_plsenter')),
            TextFormField(
              validator: (value) {
                if (value.isEmpty) {
                  return translated(context, 'setmanual_error');
                }
                return null;
              },
              textAlign: TextAlign.center,
              decoration: InputDecoration(hintText: translated(context, 'setmanual_gohere')),
              onSaved: (val) => _apiKey = val,
            ),
            RaisedButton(
              onPressed: () async {
                final FormState _form = _formKey.currentState;
                if (_form.validate()) {
                  _form.save();
                  await globals.prefs.setString('apiKey', _apiKey);
                  await globals.prefs.setString('userId', null);
                  Navigator.pop(context, _apiKey);
                }
              },
              color: Colors.purple,
              textColor: Colors.white,
              child: Text(translated(context, 'setmanual_save')),
            ),
            Text(translated(context, 'setmanual_what')),
          ],)
        )),
      ),
      onWillPop: () {
        return Future.value(this.widget.allowBack ?? false);
      },
    );
  }
}