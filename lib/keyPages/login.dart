import 'package:flutter/material.dart';
import 'package:pingpong_schedule/keyPages/keySettings.dart';
import 'package:pingpong_schedule/loading.dart';
import 'dart:async';
import 'dart:io' show Platform;
import 'package:pingpong_schedule/globals.dart' as globals;
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:pingpong_schedule/app_localizations.dart';
import 'package:pingpong_schedule/styles.dart';
import 'package:connectivity/connectivity.dart';

class Login extends StatefulWidget {
  Login({Key key, this.title, this.allowBack}) : super(key: key);

  final String title;
  final bool allowBack;

  @override
  _LoginState createState() => _LoginState();
}

//todo: uhh, spaghetti.

class _LoginState extends State<Login> {
  String _apiKey = "none detected yet...";
  String _pw;
  String _us;
  bool _dontdodumb = true;
  var a;
  bool _loggingIn = false;
  double _progress = 0;
  final _formKey = GlobalKey<FormState>();
  final flutterWebviewPlugin = new FlutterWebviewPlugin();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    if (_dontdodumb && a != null) {
      a.stopLoading(); //this might throw errors but aaaaaaaaaaaaaaaaaaaaaa
      a.close();
      a.dispose();
    }
    super.dispose();
  }

  void checkKey(a) async {
    a.evalJavascript("document.cookie").then((onValue) async {
      var hawawa = onValue.toString().split("apiKey=")[1];
      if (hawawa.contains("\"")) {
        hawawa = hawawa.substring(0,hawawa.length-1);
      }
      if (this.mounted) {
        setState(() {
          _apiKey = hawawa;
          _progress = 0;
        });
        a.cleanCookies();
        a.stopLoading();
        a.close();
        a.dispose();
        await globals.prefs.setString('apiKey', _apiKey);
        await globals.prefs.setString('userId', null);
        Navigator.pop(context, _apiKey);
      }
    });
  }

  checkOffline() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      a.cleanCookies();
      a.stopLoading();
      a.close();
      a.dispose();
      Future.delayed(const Duration(milliseconds: 250), () {
      setState(() {
        _progress = -2;
        _loggingIn = false;
      });
      });
    }
  }

  signIn(String username, String password) {
    setState(() {
      _progress = 0;
      _loggingIn = true;
    });
    Future.delayed(const Duration(milliseconds: 15000), () async {
      if (_progress > 0 && _progress < 1) {
        a.cleanCookies();
        a.stopLoading();
        a.close();
        a.dispose();
        Future.delayed(const Duration(milliseconds: 250), () {
          setState(() {
            _progress = -2;
            _loggingIn = false;
          });
        });
      }
    });
    a = new FlutterWebviewPlugin();
    a.launch("https://hjarntorget.goteborg.se/apiGetKey.do?applicationId=m.pingpong.net");
    a.cleanCookies();
    a.resize(new Rect.fromLTWH(0,0,0,0));
    checkOffline();
    a.onHttpError.listen((viewState) {
      if (_progress != 1 && !Platform.isIOS) { //iOS be like HTTP ERRO MOTHERFUCKER
      a.cleanCookies();
      a.stopLoading();
      a.close();
      a.dispose();
      Future.delayed(const Duration(milliseconds: 250), () {
      setState(() {
        _progress = -2;
        _loggingIn = false;
      });
      });
      }
    });
    a.onStateChanged.listen((viewState) {
      if (viewState.type == WebViewState.finishLoad) {
        try { if (viewState.url.substring(0,57) == "https://hjarntorget.goteborg.se/login/samlds.jsp?entityID") {
          setState(() {_progress = 0.33;});
          var _ret;
          Future.delayed(const Duration(milliseconds: 200), () async {
            _ret = await a.evalJavascript("document.getElementsByClassName('idp')[0].onclick()");
            if (_ret != null) {
              Future.delayed(const Duration(milliseconds: 800), () async {
                await a.evalJavascript("document.getElementsByClassName('idp')[0].onclick()");
              });
            }
          });
        } else if (viewState.url.substring(0,57) != "https://auth.goteborg.se/FIM/sps/Hjarntorget/saml20/login") {
          Future.delayed(const Duration(milliseconds: 2000), () { //potential ??? error handl???
            if (_progress > 0) {
              a.resize(
                new Rect.fromLTWH(
                  0.0,
                  150,
                  MediaQuery.of(context).size.width,
                  (MediaQuery.of(context).size.height) - 200,
                ),
              );
            }
          });
        }} catch(e) {
          switch (viewState.url) {
            case "https://auth.goteborg.se/auth/login":
              setState(() {_progress = 0.66;});
              var _ret;
              Future.delayed(const Duration(milliseconds: 200), () async {
                _ret = await a.evalJavascript("document.getElementById('j_username').value='" + _us + "'");
                await a.evalJavascript("document.getElementById('j_password').value='" + _pw + "'");
                await a.evalJavascript("document.getElementsByName('login')[0].onclick()");
                if (_ret != null) {
                  Future.delayed(const Duration(milliseconds: 800), () async {
                    await a.evalJavascript("document.getElementById('j_username').value='" + _us + "'");
                    await a.evalJavascript("document.getElementById('j_password').value='" + _pw + "'");
                    await a.evalJavascript("document.getElementsByName('login')[0].onclick()");
                  });
                }
              });
              break;
            case "https://auth.goteborg.se/auth/loginerrors":
              a.cleanCookies();
              a.stopLoading();
              a.close();
              a.dispose();
              setState(() {
                _progress = -1;
                _loggingIn = false;
              });
              break;
            case "https://auth.goteborg.se/auth/j_security_check":
              break;
            case "https://intranat.goteborg.se/wps/myportal":
              break; //slow internet, or something is wrong?
            case "https://m.pingpong.net/#menu":
              setState(() {_progress = 1;});
              Future.delayed(const Duration(milliseconds: 400), () {
                checkKey(a);
              });
              break;
            default:
              a.resize(
                new Rect.fromLTWH(
                  0.0,
                  200,
                  MediaQuery.of(context).size.width,
                  (MediaQuery.of(context).size.height) - 200,
                ),
              );
              break;
          }
        }
      }
    });
  }

  displayProgress() {
    return Center(
      child: LinearProgressIndicator(
        value: _progress,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(title: Text(translated(context, 'initialsetup_appbar')), automaticallyImplyLeading: this.widget.allowBack),
        body: _loggingIn ? Animate.fadeSwitcher(Center(child: pingpongLoader(context))) : Animate.fadeSwitcher(Form(
          key: _formKey,
          child: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(10, 20, 10, 10),
          child: Column( children: <Widget>[
            Image(image: AssetImage('assets/what.png'), width: 200, height: 200),
            SizedBox(height: 20),
            Text(translated(context, 'initialsetup_welcome'), textAlign: TextAlign.center, style: TextStyle(fontSize: 24)),
            Text(translated(context, _progress >= 0 ? 'initialsetup_plslogin' : _progress == -2 ? 'initialsetup_error' : 'initialsetup_incorrect'), textAlign: TextAlign.center, style: TextStyle(color: _progress <= -1 ? Colors.red : null)),
            SizedBox(height: 20),
            TextFormField(
              validator: (value) {
                if (value.isEmpty) {
                  return translated(context, 'initialsetup_empty');
                }
                if (value.contains("@")) {
                  return translated(context, 'initialsetup_notanemail') + value.split("@")[0] + "?";
                }
                return null;
              },
              textAlign: TextAlign.center,
              decoration: InputDecoration(hintText: translated(context, 'initialsetup_username'), icon: Icon(Icons.person)),
              onSaved: (val) => _us = val,
            ),
            TextFormField(
              obscureText: true,
              validator: (value) {
                if (value.isEmpty) {
                  return translated(context, 'initialsetup_empty');
                }
                return null;
              },
              textAlign: TextAlign.center,
              decoration: InputDecoration(hintText: translated(context, 'initialsetup_password'), icon: Icon(Icons.vpn_key)),
              onSaved: (val) => _pw = val,
            ),
            SizedBox(height: 20),
            GestureDetector(
              onLongPress: () async {
                String _return = await Navigator.push(context,MaterialPageRoute(builder: (BuildContext context) => KeySettings(allowBack: true)));
                if (_return != null) {
                  _dontdodumb = false;
                  Navigator.pop(context, _return);
                  return _return;
                } else {
                  return 0;
                }
              },
              child: RaisedButton(
                padding: EdgeInsets.fromLTRB(20,10,20,10),
                shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(3000000.0)),
                child: Text(translated(context, 'initialsetup_signin'), style: TextStyle(fontSize: 24)),
                onPressed: () async {
                  final FormState _form = _formKey.currentState;
                  if (_form.validate()) {
                    _form.save();
                    signIn(_us, _pw);
                  }
                },
                color: Colors.purple,
                textColor: Colors.white,
              ),
            ),
            SizedBox(height: 20),
            Text(translated(context, 'initialsetup_dw'), textAlign: TextAlign.center,),
          ],
        )),
      )),
      ),
      onWillPop: () {
        return Future.value(this.widget.allowBack ?? false);
      },
    );
  }
}
