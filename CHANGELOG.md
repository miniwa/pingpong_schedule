## [1.5.2]

* Un-break app
* Un-deadname myself

## [1.5.1]

* Profile screen
* Reply to pim
* View more messages

## [1.5.0]

* Pim
* F i n a l l y a recent flutter version
* No dependencies on weird modified libraries

## [1.4.4]

* fix the error message OH WHAT IRONY
* All the changes in 1.4.3 but also for android users

## [1.4.3]

iOS only release!!

* no more weed jokes
* app speedup by moving all the final sHaRedPReferENces = ShAreDPEFErsences:new(); to globals or whatever
* https://miniwa.space/u/acex1.jpeg
* iOS compatability

## [1.4.20]

* blazing fast performance
(i.e fix a really really dumb bug, lol)
(haha, get it? cause its 1.*4.20*)
* prevent user from trying to sign in with email like a dumdum

## [1.4.1]

* week number on all the week

## [1.4.0]

* new loading animation
* grey theme
* better looking login screen

## [1.3.0]

* forget to changelog
* cool new features

## [1.2.3]

* Fix not being able to log in if your userId is too high

## [1.2.2]

* Error handling for when hjärntorget fucks up

## [1.2.1]

* oh god

## [1.2.0]

* New login screen
* Move some assets around

## [1.1.1]

* Fix color related bugs
* More compatibilitat
* Less bad
* Copy api key by holding on edit apy key manually

## [1.1.0]

* Toggle dark mode in settings instead of only system-wide
* Add schedule FAB tooltip
* Remove question marks from settings
* Make the app title localized
* Update swedish translation
* Add a bunch more lesson icons
* Replace many lesson icons with font awesome and maki icons (and add to ossLicenses page)
* Simple course names (swedish only, probably forever)

## [1.0.4]

* This version does not exist

## [1.0.3]

* Newline tests date to prevent text overflow
* Hide fab on scroll on schedule page
* Proper placement of the snackbar on schedule page

## [1.0.2]

* Update README.md
* Thin some fonts upon request
* Fix mispelling

## [1.0.1]

* fix setting days to 0 on cancel button in date picker

## [1.0.0]

* Shows when calendar skips to monday
* No tests info
* Fix potential time zone issues
* Pretty up code a whole bunch

## [0.9.9]

* Translate app to Swedish
* Show full teacher name feature
* Tidy up code

## [0.9.8]

* Add version number in settings
* Make feature added in 0.9.7 actually work

## [0.9.7]

* Add help screen
* Add go to date feature
* Pretty code up

## [0.9.6]

* Add dates
* Add teacher names

## [0.9.5]

* Add test tracker

## [0.9.4]

* Add refresh button in automatic adder
* Unfuck accidentally fucked up animation

## [0.9.3]

* Hold to go back to initial day
* Fancy error message
* Friendlier api key screens
* Refactor and clean up some stuff

## [0.9.2]

* AndroidX fuckery
* Move a lot of stuff around
* About, share, OSS licencses screens

## [0.9.1]

* Add tests (todo) screen

## [0.9.0]

* Signed app
* App icon
* App title

## [pre-versioned]

* Everything not listed above
